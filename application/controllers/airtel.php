<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Airtel extends CI_Controller {
	public function index()
	{
		$this->load->view('airtelIndex');
	}
	public function home()
	{
		/*$this->load->model('airModel');
		$check = $this->airModel->checkUser();
		$data['check'] = $check;		//check user type(admin,reporter or supporter).......
		$this->load->view('home', $data);*/

		$this->load->view('home');
	}
	public function login()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('pass', 'Password', 'trim|required');
		if($this->form_validation->run() == false)
		{
			$this->load->view('airtelIndex');
		}
		else
		{
			if($this->input->get_post('submit'))
			{
				$data['email'] = $this->input->post('email');
				$data['pass'] = md5($this->input->post('pass'));
				
				$this->load->model('airModel');
				$result = $this->airModel->login($data);

				if($result == true)
				{
					$session = array(
						'email' => $this->input->post('email')
					);
					$this->session->set_userdata('log', $session);
					$this->load->helper('url');
					redirect('http://localhost/airtel/airtel/home', 'refresh');
				}
				else
				{
					echo "<p align=center style='border: 2px solid red;margin-left:40%;margin-right:40% ;padding:10px;margin-top:20%;border-radius:5px;font-size:22px;color:red;font-family: 'Times New Roman', Times, serif;color:#ffffb3;';>ID and Password not matched!<a href=http://localhost/airtel/airtel/login> Try again</a></p>";
				}
			}
		}
	}


	//report view start from here.......
	public function status_report()
	{
		$this->load->model('airModel');
		$data['sts_rep'] = $this->airModel->mstatus_report();
		$this->load->library('parser');
		$this->parser->parse('status_report', $data);
	}
	public function service_report()
	{		
		if(empty($this->input->get_post('submit')))
		{
			$this->load->view('service_src');
		}
		else if($this->input->get_post('submit'))
		{
			$data['keyword'] = $this->input->post('keyword');
			$data['shcode'] = $this->input->post('shcode');
			$data['type'] = $this->input->post('type');
			$data['pid'] = $this->input->post('pid');
			$data['sid'] = $this->input->post('sid');

			$service = array(
				'keyword' => $this->input->post('keyword'),
				'shcode' => $this->input->post('shcode'),
				'type' => $this->input->post('type'),
				'pid' => $this->input->post('pid'),
				'sid' => $this->input->post('sid')
			);
			$this->session->set_userdata('service', $service);

			$this->load->model('airModel');
			$data['srvc_src'] = $this->airModel->service_search($data);
			$this->load->library('parser');
			$this->parser->parse('service_src', $data);
		}
	}
	public function callback_report()
	{
		if(empty($this->input->get_post('submit')))
		{
			$this->load->view('callback_src');
		}
		else if($this->input->get_post('submit'))
		{
			$data['msisdn'] = $this->input->post('msisdn');
			$data['sid'] = $this->input->post('sid');
			$data['pid'] = $this->input->post('pid');
			$data['stype'] = $this->input->post('stype');

			$callback = array(
				'msisdn' => $this->input->post('msisdn'),
				'sid' => $this->input->post('sid'),
				'pid' => $this->input->post('pid'),
				'stype' => $this->input->post('stype')
			);
			$this->session->set_userdata('callback', $callback);

			$this->load->model('airModel');
			$data['calbk_src'] = $this->airModel->callback_search($data);
			$this->load->library('parser');
			$this->parser->parse('callback_src', $data);
		}
	}
	public function request_report()
	{
		if(empty($this->input->get_post('submit')))
		{
			$this->load->view('request_src');
		}
		else if($this->input->get_post('submit'))
		{
			$data['msisdn'] = $this->input->post('msisdn');
			$data['pid'] = $this->input->post('pid');
			$data['hitdate'] = $this->input->post('hitdate');
			$data['resdate'] = $this->input->post('resdate');

			$request = array(
				'msisdn' => $this->input->post('msisdn'),
				'pid' => $this->input->post('pid'),
				'hitdate' => $this->input->post('hitdate'),
				'resdate' => $this->input->post('resdate')
			);
			$this->session->set_userdata('request', $request);

			$this->load->model('airModel');
			$data['req_src'] = $this->airModel->request_search($data);
			$this->load->library('parser');
			$this->parser->parse('request_src', $data);
		}
	}
	public function subscriber_report()
	{
		if(empty($this->input->get_post('submit')))
		{
			$this->load->view('subscriber_src');
		}
		else if($this->input->get_post('submit'))
		{
			$data['msisdn'] = $this->input->post('msisdn');
			$data['pid'] = $this->input->post('pid');
			$data['keyword'] = $this->input->post('keyword');
			$data['stype'] = $this->input->post('stype');

			$subscriber = array(
				'msisdn' => $this->input->post('msisdn'),
				'pid' => $this->input->post('pid'),
				'keyword' => $this->input->post('keyword'),
				'stype' => $this->input->post('stype')
			);
			$this->session->set_userdata('subscriber', $subscriber);

			$this->load->model('airModel');
			$data['subs_src'] = $this->airModel->subscriber_search($data);
			$this->load->library('parser');
			$this->parser->parse('subscriber_src', $data);
		}
	}//report view end........


	//create view start from here........
	public function create_status()
	{
		//$this->load->view('status_insert'); //status
		$this->form_validation->set_rules('asecode', 'Ase Code', 'trim|required');
		$this->form_validation->set_rules('sslcode', 'SSL Code', 'trim|required');
		$this->form_validation->set_rules('details', 'Details', 'trim|required');
		if($this->form_validation->run() == false)
		{
			$this->load->view('status_insert');
		}
		else
		{
			if($this->input->get_post('submit'))
			{
				$data['asecode'] = $this->input->post('asecode');
				$data['sslcode'] = $this->input->post('sslcode');
				$data['details'] = $this->input->post('details');
				
				$this->load->model('airModel');
				$result = $this->airModel->ststus_ins($data);
				$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
				  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  	<strong>Well done!</strong> New Record Inserted.
				</div>";
				$sess = array(
						'suc' => $msg
				);
				$this->session->set_userdata('sucMsg', $sess);
				$this->load->helper('url');
				redirect('http://localhost/airtel/airtel/home', 'refresh');
			}
		}
	}
	public function create_service()//service
	{
		//$this->load->view('service_insert'); 
		$this->form_validation->set_rules('sname', 'Service Name', 'trim|required');
		$this->form_validation->set_rules('keyword', 'Keyword', 'trim|required');
		$this->form_validation->set_rules('bprice', 'Base Price', 'trim|required');
		$this->form_validation->set_rules('shcode', 'Short Code', 'trim|required');
		$this->form_validation->set_rules('snode', 'Service Node', 'trim|required');
		$this->form_validation->set_rules('pid', 'Plan ID', 'trim|required');
		$this->form_validation->set_rules('stype', 'Service Yype', 'trim|required');
		$this->form_validation->set_rules('plndesc', 'Plan Description', 'trim|required');
		$this->form_validation->set_rules('activday', 'Active Day', 'trim|required');
		$this->form_validation->set_rules('sid', 'Service ID', 'trim|required');
		if($this->form_validation->run() == false)
		{
			$this->load->view('service_insert');
		}
		else
		{
			if($this->input->get_post('submit'))
			{
				$data['sname'] = $this->input->post('sname');
				$data['keyword'] = $this->input->post('keyword');
				$data['bprice'] = $this->input->post('bprice');
				$data['shcode'] = $this->input->post('shcode');
				$data['snode'] = $this->input->post('snode');
				$data['pid'] = $this->input->post('pid');
				$data['stype'] = $this->input->post('stype');
				$data['plndesc'] = $this->input->post('plndesc');
				$data['activday'] = $this->input->post('activday');
				$data['sid'] = $this->input->post('sid');
				
				$this->load->model('airModel');
				$result = $this->airModel->service_ins($data);
				$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
				  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  	<strong>Well done!</strong> New Record Inserted.
				</div>";
				$sess = array(
						'suc' => $msg
				);
				$this->session->set_userdata('sucMsg', $sess);
				$this->load->helper('url');
				redirect('http://localhost/airtel/airtel/home', 'refresh');
			}
		}
	}//create voew end.......


	//update list view start from here.......
	public function status_up_list()
	{
		$this->load->model('airModel');
		$data['sts_upl'] = $this->airModel->stat_upl();
		$this->load->library('parser');
		$this->parser->parse('status_update', $data);
	}
	public function service_up_list()
	{
		$this->load->model('airModel');
		$data['serv_upl'] = $this->airModel->servc_upl();
		$this->load->library('parser');
		$this->parser->parse('service_update', $data);
	}
	public function subscriber_up_list()
	{
		$this->load->model('airModel');
		$data['sub_upl'] = $this->airModel->subs_upl();
		$this->load->library('parser');
		$this->parser->parse('subscribers_update', $data);
	}//update list view end....

	//update confirm start from here....
	public function status_upconf($id) //status update
	{
		if(empty($this->input->get_post('submit')))
		{
			$this->load->model('airModel');
			$data = $this->airModel->status_up_data($id);
			$this->load->library('parser');
			$this->parser->parse('status_up_conf', $data);
		}
		else if($this->input->get_post('submit'))
		{
			$data['ids'] = $this->input->post('ids');
			$data['asecode'] = $this->input->post('asecode');
			$data['sslcode'] = $this->input->post('sslcode');
			$data['details'] = $this->input->post('details');

			$this->load->model('airModel');
			$this->airModel->status_upconf($data);

			$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
				  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  	<strong>Well done!</strong> Data Updated Successfuly.
				</div>";
				$sess = array(
						'suc' => $msg
				);
			$this->session->set_userdata('sucMsg', $sess);
			
			$this->load->helper('url');
			redirect('http://localhost/airtel/airtel/home', 'refresh');
		}
	}
	public function service_upconf($id) //service update
	{
		if(empty($this->input->get_post('submit')))
		{
			$this->load->model('airModel');
			$data = $this->airModel->service_up_data($id);
			$this->load->library('parser');
			$this->parser->parse('service_up_conf', $data);
		}
		else if($this->input->get_post('submit'))
		{
			$data['ids'] = $this->input->post('ids');
			$data['sname'] = $this->input->post('sname');
			$data['keyword'] = $this->input->post('keyword');
			$data['bprice'] = $this->input->post('bprice');
			$data['shcode'] = $this->input->post('shcode');
			$data['snode'] = $this->input->post('snode');
			$data['pid'] = $this->input->post('pid');
			$data['stype'] = $this->input->post('stype');
			$data['activday'] = $this->input->post('activday');
			$data['sid'] = $this->input->post('sid');

			$this->load->model('airModel');
			$this->airModel->service_upconf($data);
			$this->load->helper('url');

			$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
				  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  	<strong>Well done!</strong> Data Updated Successfuly.
				</div>";
				$sess = array(
						'suc' => $msg
				);
			$this->session->set_userdata('sucMsg', $sess);
			
			redirect('http://localhost/airtel/airtel/home', 'refresh');
		}
	}
	public function subscriber_upconf($id) //subscriber update
	{
		if(empty($this->input->get_post('submit')))
		{
			$this->load->model('airModel');
			$data = $this->airModel->subscriber_up_data($id);
			$this->load->library('parser');
			$this->parser->parse('subscriber_up_conf', $data);
		}
		else if($this->input->get_post('submit'))
		{
			$data['ids'] = $this->input->post('ids');
			$data['keyword'] = $this->input->post('keyword');
			$data['shcode'] = $this->input->post('shcode');
			$data['instatus'] = $this->input->post('instatus');
			$data['pstatus'] = $this->input->post('pstatus');
			$data['cstatus'] = $this->input->post('cstatus');
			$data['fstatus'] = $this->input->post('fstatus');
		
			$this->load->model('airModel');
			$this->airModel->subscriber_upconf($data);

			$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
				  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  	<strong>Well done!</strong> Data Updated Successfuly.
				</div>";
				$sess = array(
						'suc' => $msg
				);
			$this->session->set_userdata('sucMsg', $sess);
			
			$this->load->helper('url');
			redirect('http://localhost/airtel/airtel/home', 'refresh');
		}
	}//update confirm end......


	//delete list view start from here.....
	public function status_del_list()
	{
		$this->load->model('airModel');
		$data['sts_del'] = $this->airModel->stat_upl();
		$this->load->library('parser');
		$this->parser->parse('status_del', $data);
	}
	public function service_del_list()
	{
		$this->load->model('airModel');
		$data['serv_del'] = $this->airModel->servc_upl();
		$this->load->library('parser');
		$this->parser->parse('service_del', $data);
	}//delete list end.....

	//delete confirm start from here.....
	public function status_del_conf($id)
	{
		$this->load->model('airModel');
		$this->airModel->status_delconf($id);

		$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
			  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
			  	<strong>Well done!</strong> Data Deleted Successfuly.
			</div>";
			$sess = array(
					'suc' => $msg
			);
		$this->session->set_userdata('sucMsg', $sess);
			
		$this->load->helper('url');
		redirect('http://localhost/airtel/airtel/status_report', 'refresh');
	}
	public function service_del_conf($id)
	{
		$this->load->model('airModel');
		$this->airModel->service_delconf($id);
		
		$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
			  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
			  	<strong>Well done!</strong> Data Deleted Successfuly.
			</div>";
		$sess = array(
				'suc' => $msg
		);
		$this->session->set_userdata('sucMsg', $sess);

		$this->load->helper('url');
		redirect('http://localhost/airtel/airtel/home', 'refresh');
	}//delete confirm end

	public function addUser()
	{
		//$this->load->view('add_user');
		$this->form_validation->set_rules('fname', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('type', 'User Type', 'trim|required');
		$this->form_validation->set_rules('pass', 'Password', 'trim|required');
		if($this->form_validation->run() == false)
		{
			$this->load->view('add_user');
		}
		else
		{
			if($this->input->get_post('submit'))
			{
				$data['fname'] = $this->input->post('fname');
				$data['email'] = $this->input->post('email');
				$data['type'] = $this->input->post('type');
				$data['pass'] = md5($this->input->post('pass'));
				
				$this->load->model('airModel');
				$result = $this->airModel->addUser($data);
				$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
				  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				  	<strong>Well done!</strong> New User Added Successfuly.
				</div>";
				$sess = array(
						'suc' => $msg
				);
				$this->session->set_userdata('sucMsg', $sess);
				$this->load->helper('url');
				redirect('http://localhost/airtel/airtel/home', 'refresh');
			}
		}
	}

	//excel start from here 
	public function service_excel()//service excel
	{
		if($this->input->get_post('xl'))
		{
			$this->load->model('airModel');
			$data['srvc_src'] = $this->airModel->service_exc();
			$this->load->library('parser');
			$this->parser->parse('service_exc', $data);
		}
	}
	public function callback_excel()//callback excel
	{
		$this->load->model('airModel');
		$data['calbk_src'] = $this->airModel->callback_exc();
		$this->load->library('parser');
		$this->parser->parse('callback_exc', $data);
	}
	public function request_excel()//request excel
	{
		$this->load->model('airModel');
		$data['req_src'] = $this->airModel->request_exc();
		$this->load->library('parser');
		$this->parser->parse('request_exc', $data);
	}
	public function subscriber_excel()//subscriber excel
	{
		$this->load->model('airModel');
		$data['subs_src'] = $this->airModel->subscriber_exc();
		$this->load->library('parser');
		$this->parser->parse('subscriber_exc', $data);
	}//excel end


	//pdf report start from here
	public function service_pdf()//service pdf
	{
		$this->load->model('airModel');
		$srvc_src = $this->airModel->service_pdf();
		$data['srvc_src'] = $srvc_src;
		$this->load->view('service_pdf', $data);
	}
	public function callback_pdf()//callback pdf
	{
		$this->load->model('airModel');
		$calbk_src = $this->airModel->callback_exc();
		$data['calbk_src'] = $calbk_src;
		$this->load->view('callback_pdf', $data);
	}
	public function request_pdf()//request pdf
	{
		$this->load->model('airModel');
		$req_src = $this->airModel->request_exc();
		$data['req_src'] = $req_src;
		$this->load->view('request_pdf', $data);
	}
	public function subscriber_pdf()//subscriber pdf
	{
		$this->load->model('airModel');
		$subs_src = $this->airModel->subscriber_exc();
		$data['subs_src'] = $subs_src;
		$this->load->view('subscriber_pdf', $data);
	}//pdf end


	//update password
	public function update_pass()
	{
		$this->load->view('pass_up');
		
		if($this->input->get_post('submit'))
		{
			$data['npass'] = md5($this->input->post('npass'));
			
			$this->load->model('airModel');
			$result = $this->airModel->password_update($data);
			
			$msg = "<div class='alert alert-success alert-dismissible' role='alert'>
			  	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
			  	<strong>Well done!</strong> Password Updated.
			</div>";
			$sess = array(
					'suc' => $msg
			);
			$this->session->set_userdata('sucMsg', $sess);
			$this->load->helper('url');
			redirect('http://localhost/airtel/airtel/home', 'refresh');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->load->helper('url');
		redirect('http://localhost/airtel/', 'refresh');
	}
}
