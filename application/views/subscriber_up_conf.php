<?php
  include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Update Subscribers</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
<div class="container">
    <h1>Update Subscribers</h1>
    <div class="row">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
          <div class="col-md-6">
          		<div class="form-group">
                  	<label>ID:</label>
                  	<input type="text" name="ids" class="form-control" required readonly value="{id}">
              	</div>
              <div class="form-group">
                  <label>Keyword:</label>
                  <input type="text" name="keyword" class="form-control" required value="{keyword}">
              </div>
              <div class="form-group">
                  <label>Short Code:</label>
                  <input type="text" name="shcode" class="form-control" required value="{shortcode}">
              </div>
              <div class="form-group">
                  <label>Initial Status:</label>
                  <input type="text" name="instatus" class="form-control" required value="{initial_status}">
              </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                  <label>Previous Status:</label>
                  <input type="text" name="pstatus" class="form-control" required value="{previous_status}">
              </div>
            <div class="form-group">
                  <label>Current Status:</label>
                  <input type="text" name="cstatus" class="form-control" required value="{current_status}">
              </div>
              <div class="form-group">
                  <label>Final Status:</label>
                  <input type="text" name="fstatus" class="form-control" required value="{final_status}">
              </div>
              <input type="submit" name="submit" class="btn btn-default" value="Update"><a href="http://localhost/airtel/airtel/subscriber_report" align="center">Back</a>
          </div>
      </form> 
    </div>
</div>
</body>
</html>