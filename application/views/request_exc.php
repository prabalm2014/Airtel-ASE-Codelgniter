<?php
	if(isset($_POST["xl"]))
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$output .='
			<h2 align="center">Request Report</h2>
			<table class="table" borddered="1">
				<tr>
					<th>ID</th>
			       	<th>Msisdn</th>
			       	<th>Keyword</th>
			       	<th>Plan ID</th>
			       	<th>Request Type</th>
			       	<th>Request Data</th>
			       	<th>Response Data</th>
			       	<th>Hit Time</th>
			       	<th>Response Time</th>
				</tr>
		';
		$output .='
			{req_src}
				<tr>
					<td>{id}</td>
					<td>{msisdn}</td>
					<td>{keyword}</td>
					<td>{plan_id}</td>
					<td>{request_type}</td>
					<td>{request_data}</td>
					<td>{response_data}</td>
					<td>{hittime}</td>
					<td>{response_time}</td>
				</tr>
			{/req_src}
		';
		$output .='</table>';
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=request.xls");
		echo $output;

		$data = $this->session->userdata('request');
		if(!empty($data['msisdn']) && !empty($data['pid']) && !empty($data['hitdate']) && !empty($data['resdate']))
		{
			$this->session->unset_userdata('request');
		}
	}
?>