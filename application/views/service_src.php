<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Service Report</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<!-- for search -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
		    	<h2>Service Report</h2>
		    </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="">
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="shcode" class="form-control" placeholder="Short Code">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<select name="type" class="form-control">
							<option value="" disabled="disabled" selected>Service Type</option>
							<option value="call">CALL</option>
							<option value="sms">SMS</option>
							<option value="voice sms">VOICE SMS</option>
						</select>
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="sid" class="form-control" placeholder="Service ID">
					</div>
	        	</div>
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	    <!-- search result -->

	    <?php
	    if(empty($this->input->get_post('submit')))
	    {
	    	echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Short Code,Service Type,Plan ID should be accurate.</em></li>
					</ul> 
	      		</div>";
	    }
	    ?>
	    <?php 
	    if($this->input->get_post('submit'))
	    {
	    	echo "<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Service Name</th>
				       	<th>Keyword</th>
				       	<th>Base Price</th>
				       	<th>Short Code</th>
				       	<th>Service Node</th>
				       	<th>Plan ID</th>
				       	<th>Service Type</th>
				       	<th>Plan Description</th>
				       	<th>Active Day</th>
				       	<th>Active?</th>
				       	<th>Service ID</th>";
				       	if(!empty($data['email']) && ($row['type'] == 'admin'))
						{
				       		echo '<th>Action</th>';
				       	}
				   echo " </tr>
			    </thead>
				<tbody>

				{srvc_src}
					<tr>
						<td>{id}</td>
						<td>{service_name}</td>
						<td>{keyword}</td>
						<td>{base_price}</td>
						<td>{shortcode}</td>
						<td>{service_node}</td>
						<td>{plan_id}</td>
						<td>{service_type}</td>
						<td>{plan_desc}</td>
						<td>{active_days}</td>
						<td>{is_active}</td>
						<td>{service_id}</td>";
						if(!empty($data['email']) && ($row['type'] == 'admin'))
						{
							echo "<td><a href='http://localhost/airtel/airtel/service_upconf/{id}'>Update</a> | <a href='http://localhost/airtel/airtel/service_del_conf/{id}'>Delete</a></td>";
						}
					echo "</tr>
					{/srvc_src}
				</tbody>
			</table>";}
	   	?>
	   		<form action="http://localhost/airtel/airtel/service_excel" method="post">
				<input type="submit" name="xl" class="btn btn-success" value="Excel">
				<a href='http://localhost/airtel/airtel/service_pdf' class="btn btn-success">PDF</a>
				<a href='http://localhost/airtel/airtel/home' class="btn btn-primary">Back</a>
			</form>
		</div>
	</div>
</body>
</html>