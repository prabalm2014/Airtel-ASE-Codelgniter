<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
	
	<!-- Customized Java Script and CSS -->
	
	<script type="text/javascript" src="http://localhost/itnews/public/js.js"></script>
	<script type="text/javascript" src="http://localhost/itnews/public/index.js"></script>
	<link rel="stylesheet" href="http://localhost/itnews/public/style1.css" type="text/css">
</head>
<body>
	<form method="post" action="/airtel/airtel/login" name="login" id="login">
	<h1>Log In</h1><br/>
		<div class="group">
			<input type="text" name="email"  value="<?php echo set_value('email'); ?>" id="username"><span class="highlight"></span><span class="bar"></span>
			<label>Username</label>
		</div>
		<div class="group">
			<input type="password" name="pass" value="" id="password"><span class="highlight"></span><span class="bar"></span>
			<label>Password</label>
		</div>
		<span style="color:red;"><?php echo validation_errors(); ?></span>
		<input type="submit" name="submit" id="submit" class="button buttonBlue" value="Log In">
	</form>
	<footer>
		<a href="">Airtel</a> &#169 2016</p>
	</footer>

</body>
</html>