<?php
if(isset($_POST["xl"]))
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$output .='
			<h2 align="center">Callback Report</h2>
			<table class="table" borddered="1">
				<tr>
					<th>ID</th>
			       	<th>Msisdn</th>
			       	<th>Service ID</th>
			       	<th>Service Type</th>
			       	<th>Plan ID</td>
			       	<th>Error Code</th>
			       	<th>Operation</th>
			       	<th>Result</th>
			       	<th>Trans ID</th>
			       	<th>Content ID</th>
			       	<th>Category</th>
			       	<th>Charge Amount</th>
			       	<th>Applied Plan</th>
			       	<th>End Date</th>
			       	<th>Validity Days</th>
			       	<th>Remarks</th>
			       	<th>Incomming Time</th>
			       	<th>Raw Data</th>
				</tr>
		';
			$output .='
				{calbk_src}
					<tr>
						<td>{id}</td>
						<td>{msisdn}</td>
						<td>{serviceId}</td>
						<td>{serviceType}</td>
						<td>{planId}</td>
						<td>{errorCode}</td>
						<td>{operation}</td>
						<td>{result}</td>
						<td>{transId}</td>
						<td>{contentId}</td>
						<td>{category}</td>
						<td>{chargeAmount}</td>
						<td>{appliedPlan}</td>
						<td>{endDate}</td>
						<td>{validityDays}</td>
						<td>{remarks}</td>
						<td>{incomming_time}</td>
						<td>{raw_data}</td>
					</tr>
				{/calbk_src}
			';
		$output .='</table>';
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=callback.xls");
		echo $output;

		$data = $this->session->userdata('callback');
		if(!empty($data['msisdn']) && !empty($data['sid']) && !empty($data['pid']) && !empty($data['stype']))
		{
			$this->session->unset_userdata('callback');
		}
	}

?>