<?php 
	if(isset($_POST["xl"]))
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$output .='
			<h2 align="center">Service Report</h2>
			<table class="table" borddered="1">
				<tr>
					<th>ID</th>
					<th>Service Name</th>
			       	<th>Keyword</th>
			       	<th>Base Price</th>
			       	<th>Short Code</th>
			       	<th>Service Node</th>
			       	<th>Plan ID</th>
			       	<th>Service Type</th>
			       	<th>Plan Description</th>
			       	<th>Active Day</th>
			       	<th>Active?</th>
			       	<th>Service ID</th>
				</tr>
		';
			$output .='
				{srvc_src}
					<tr>
						<td>{id}</td>
						<td>{service_name}</td>
						<td>{keyword}</td>
						<td>{base_price}</td>
						<td>{shortcode}</td>
						<td>{service_node}</td>
						<td>{plan_id}</td>
						<td>{service_type}</td>
						<td>{plan_desc}</td>
						<td>{active_days}</td>
						<td>{is_active}</td>
						<td>{service_id}</td>
					</tr>
					{/srvc_src}
			';
		$output .='</table>';
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=service.xls");
		echo $output;

		$data = $this->session->userdata('service');
		if(!empty($data['keyword']) && !empty($data['shcode']) && !empty($data['type']) && !empty($data['pid']) && !empty($data['sid']))
		{
			$this->session->unset_userdata('service');
		}
	}

?>