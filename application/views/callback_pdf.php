<?php
	require("pdf/fpdf.php");
	$pdf = new FPDF('P','mm','A4');
	$pdf->SetMargins(0,0);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	

	$pdf->SetFont("Arial","B",12);
	$pdf->Cell(0,10,"Callback Report",0,1,'C');
	//$pdf->Ln();

	$pdf->SetFont("Arial","B",8);
	$pdf->Cell(7,15,"ID",1,0,'C');
	$pdf->Cell(11,15,"Msisdn",1,0,'C');
	$pdf->Cell(11,15,"Serv. ID",1,0,'C');
	$pdf->Cell(14,15,"Serv. Type",1,0,'C');
	$pdf->Cell(11,15,"Plan ID",1,0,'C');
	$pdf->Cell(15,15,"Error Code",1,0,'C');
	$pdf->Cell(14,15,"Operation",1,0,'C');
	$pdf->Cell(9,15,"Result",1,0,'C');
	$pdf->Cell(12,15,"Trans ID",1,0,'C');
	$pdf->Cell(14,15,"Content ID",1,0,'C');
	$pdf->Cell(13,15,"Category",1,0,'C');
	$pdf->Cell(16,15,"ChargeAmo.",1,0,'C');
	$pdf->Cell(15,15,"App. Plan",1,0,'C');
	$pdf->Cell(12,15,"EndDate",1,0,'C');
	$pdf->Cell(13,15,"ValidDay",1,0,'C');
	$pdf->Cell(11,15,"Remark",1,0,'C');
	$pdf->Cell(11,15,"Ic.Time",1,1,'C');

	foreach($calbk_src as $row)
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$pdf->SetFont("Arial","B",8);
		$pdf->Cell(7,10,$row["id"],1,0,'C');
		$pdf->Cell(11,10,$row["msisdn"],1,0,'C');
		$pdf->Cell(11,10,$row["serviceId"],1,0,'C');
		$pdf->Cell(14,10,$row["serviceType"],1,0,'C');
		$pdf->Cell(11,10,$row["planId"],1,0,'C');
		$pdf->Cell(15,10,$row["errorCode"],1,0,'C');
		$pdf->Cell(14,10,$row["operation"],1,0,'C');
		$pdf->Cell(9,10,$row["result"],1,0,'C');
		$pdf->Cell(12,10,$row["transId"],1,0,'C');
		$pdf->Cell(14,10,$row["contentId"],1,0,'C');
		$pdf->Cell(13,10,$row["category"],1,0,'C');
		$pdf->Cell(16,10,$row["chargeAmount"],1,0,'C');
		$pdf->Cell(15,10,$row["appliedPlan"],1,0,'C');
		$pdf->Cell(12,10,$row["endDate"],1,0,'C');
		$pdf->Cell(13,10,$row["validityDays"],1,0,'C');
		$pdf->Cell(11,10,$row["remarks"],1,0,'C');
		$pdf->Cell(11,10,$row["incomming_time"],1,1,'C');
	}

	$pdf->Ln();
	$pdf->Ln();

	$pdf->Output();
?>