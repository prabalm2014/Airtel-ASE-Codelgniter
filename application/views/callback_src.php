<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Call Back Report</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<!-- for search -->
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-1">
		    	<h2>Call Back Report</h2>
		    </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="">
	        	<div class="col-md-2 col-md-offset-1">
					<div class="form-group">
						<input type="text" name="msisdn" class="form-control" placeholder="Msisdn">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="sid" class="form-control" placeholder="Service ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<select name="stype" class="form-control">
							<option value="" disabled="disabled" selected>Service Type</option>
							<option value="call">CALL</option>
							<option value="sms">SMS</option>
							<option value="voice sms">VOICE SMS</option>
						</select>
					</div>
	        	</div>
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	    </div>
	    <!-- search result -->
	    <div class="container-fluid">
	    <?php
	    if(empty($this->input->get_post('submit')))
	    {
	    	echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Short Code,Service Type,Plan ID should be accurate.</em></li>
					</ul> 
	      		</div>";
	    }
	    ?>
	    <?php 
	    if($this->input->get_post('submit'))
	    {
	    	echo "<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Service ID</th>
				       	<th>Service Type</th>
				       	<th>Plan ID</td>
				       	<th>Error Code</th>
				       	<th>Operation</th>
				       	<th>Result</th>
				       	<th>Trans ID</th>
				       	<th>Content ID</th>
				       	<th>Category</th>
				       	<th>Charge Amount</th>
				       	<th>Applied Plan</th>
				       	<th>End Date</th>
				       	<th>Validity Days</th>
				       	<th>Remarks</th>
				       	<th>Incomming Time</th>
				       	<th>Raw Data</th>
				    </tr>
			    </thead>
				<tbody>

				{calbk_src}
					<tr>
						<td>{id}</td>
						<td>{msisdn}</td>
						<td>{serviceId}</td>
						<td>{serviceType}</td>
						<td>{planId}</td>
						<td>{errorCode}</td>
						<td>{operation}</td>
						<td>{result}</td>
						<td>{transId}</td>
						<td>{contentId}</td>
						<td>{category}</td>
						<td>{chargeAmount}</td>
						<td>{appliedPlan}</td>
						<td>{endDate}</td>
						<td>{validityDays}</td>
						<td>{remarks}</td>
						<td>{incomming_time}</td>
						<td>{raw_data}</td>
					</tr>
					{/calbk_src}
				</tbody>
			</table>";}
	   	?>
	   	<form action="http://localhost/airtel/airtel/callback_excel" method="post">
			<input type="submit" name="xl" class="btn btn-success" value="Excel">
			<a href='http://localhost/airtel/airtel/callback_pdf' class="btn btn-success">PDF</a>
			<a href='http://localhost/airtel/airtel/home' class="btn btn-primary">Back</a>
		</form>
	</div>
	</div>
</body>
</html>