<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Request Logs Report</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
	<script>
		$( function() {
			$( "#datepicker" ).datepicker({dateFormat:"yy-mm-dd"});
			$( "#datepicker2" ).datepicker({dateFormat:"yy-mm-dd"});
		} );
	</script>
</head>
<body>
	<!-- for search -->
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-1">
		    		<h2>Request Logs Report</h2>
		        </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="">
	        	<div class="col-md-2 col-md-offset-1">
					<div class="form-group">
						<input type="text" name="msisdn" class="form-control" placeholder="Msisdn">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="hitdate" class="form-control" id="datepicker" placeholder="Hit Date">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="resdate" class="form-control" id="datepicker2" placeholder="Response Date">
					</div>
	        	</div>
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	    <!-- search result -->
	    <?php
	    if(empty($this->input->get_post('submit')))
	    {
	    	echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Plan ID,Hit Date,Response Date should be accurate.</em></li>
					</ul> 
	      		</div>";
	    }
	    ?>
	    <?php 
	    if($this->input->get_post('submit'))
	    {
	    	echo "<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Keyword</th>
				       	<th>Plan ID</th>
				       	<th>Request Type</th>
				       	<th>Request Data</th>
				       	<th>Response Data</th>
				       	<th>Hit Time</th>
				       	<th>Response Time</th>
				    </tr>
			    </thead>
				<tbody>

				{req_src}
					<tr>
						<td>{id}</td>
						<td>{msisdn}</td>
						<td>{keyword}</td>
						<td>{plan_id}</td>
						<td>{request_type}</td>
						<td>{request_data}</td>
						<td>{response_data}</td>
						<td>{hittime}</td>
						<td>{response_time}</td>
					</tr>
					{/req_src}
				</tbody>
			</table>";}
	   	?>
	   	<form action="http://localhost/airtel/airtel/request_excel" method="post">
			<input type="submit" name="xl" class="btn btn-success" value="Excel">
			<a href='http://localhost/airtel/airtel/request_pdf' class="btn btn-success">PDF</a>
			<a href='http://localhost/airtel/airtel/home' class="btn btn-primary">Back</a>
		</form>
	</div>
</body>
</html>