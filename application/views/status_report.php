<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<!--<title>Status Report</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Status Report</h1></div>
			<div class="panel-body">
				<table class="table table-striped">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Code ASE</th>
				       	<th>Code SSL</th>
				       	<th>Details</th>
				       	<?php 
				       	if(!empty($data['email']) && ($row['type'] == 'admin'))
				       	{
				       		echo "<th>Actions</th>";
				       	}?> 	
				    </tr>
			    </thead>
			    <tbody>
			    	{sts_rep}
					<tr>
						<td>{id}</td>
						<td>{status_code_ase}</td>
						<td>{status_code_ssl}</td>
						<td>{detaiuls}</td>
						<?php 
				       	if(!empty($data['email']) && ($row['type'] == 'admin'))
				       	{
				       		echo '<td><a href="http://localhost/airtel/airtel/status_upconf/{id}">Update</a> | <a href="http://localhost/airtel/airtel/status_del_conf/{id}">Delete</a></td>';
				       	}?>
					</tr>
					{/sts_rep}
			    </tbody>
			  	</table>
			  	<a href="http://localhost/airtel/airtel/home" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>