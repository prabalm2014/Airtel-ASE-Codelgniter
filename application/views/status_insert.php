<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Create Status</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<div class="container">
    <h1>Create Status</h1>
	    <div class="row">
	        <form method="post" action="/airtel/airtel/create_status">
	          	<div class="col-md-6">
	              	<div class="form-group">
	                  	<label for="asecode">ASE Code:</label>
	                  	<input type="text" name="asecode" class="form-control">
	              	</div>
	              	<div class="form-group">
	                  	<label for="sslcode">SSL Code:</label>
	                  	<input type="text" name="sslcode" class="form-control">
	              	</div>
	              	<div class="form-group">
	                  	<label for="details">Details:</label>
	                  	<input type="text" name="details" class="form-control">
	              	</div>
	              	<span style="color:red;"><?php echo validation_errors(); ?></span>
	              	<input type="submit" name="submit" class="btn btn-default" value="Submit"><a href="http://localhost/airtel/airtel/home" align="center">Back</a>
	          	</div>
	      	</form> 
	    </div>
	</div>
</body>
</html>