<?php
	if(isset($_POST["xl"]))
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$output .='
			<h2 align="center">Subscriber Report</h2>
			<table class="table" borddered="1">
				<tr>
					<th>ID</th>
			       	<th>Msisdn</th>
			       	<th>Plan ID</th>
			       	<th>Keyword</th>
			       	<th>Short Code</th>
			       	<th>Initial Status</th>
			       	<th>Previous Status</th>
			       	<th>Current Status</th>
			       	<th>Final Status</th>
			       	<th>Incomming Time</th>
			       	<th>Last Update Time</th>
			       	<th>Status Update Time</th>
			       	<th>Service Type</th>
			       	<th>Remarks</th>
			       	<th>Count</th>
				</tr>
		';
		$output .='
			{subs_src}
			<tr>
				<td>{id}</td>
				<td>{msisdn}</td>
				<td>{plan_id}</td>
				<td>{keyword}</td>
				<td>{shortcode}</td>
				<td>{initial_status}</td>
				<td>{previous_status}</td>
				<td>{current_status}</td>
				<td>{final_status}</td>
				<td>{incomming_time}</td>
				<td>{last_update_time}</td>
				<td>{status_update_time}</td>
				<td>{service_type}</td>
				<td>{remarks}</td>
				<td>{count}</td>
			</tr>
			{/subs_src}
		';
		$output .='</table>';
		header("Content-Type: application/xls");
		header("Content-Disposition: attachment; filename=subscriber.xls");
		echo $output;

		$data = $this->session->userdata('subscriber'); 
		if(!empty($data['msisdn']) && !empty($data['pid']) && !empty($data['keyword']) && !empty($data['stype']))
		{
			$this->session->unset_userdata('subscriber');
		}
	}
?>