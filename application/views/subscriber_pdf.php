<?php
	require("pdf/fpdf.php");
	$pdf = new FPDF('P','mm','A4');
	$pdf->SetMargins(0,0);
	$pdf->AliasNbPages();
	$pdf->AddPage();

	$pdf->SetFont("Arial","B",12);
	$pdf->Cell(0,10,"Subscribers Report",0,1,'C');
	//$pdf->Ln();

	$pdf->SetFont("Arial","B",8);
	$pdf->Cell(7,15,"ID",1,0,'C');
	$pdf->Cell(11,15,"Msisdn",1,0,'C');
	$pdf->Cell(11,15,"Plan ID",1,0,'C');
	$pdf->Cell(13,15,"Keyword",1,0,'C');
	$pdf->Cell(15,15,"Short Code",1,0,'C');
	$pdf->Cell(15,15,"Init. Status",1,0,'C');
	$pdf->Cell(16,15,"Prev. Status",1,0,'C');
	$pdf->Cell(17,15,"Curn. Status",1,0,'C');
	$pdf->Cell(16,15,"Final Status",1,0,'C');
	$pdf->Cell(17,15,"Incom. Time",1,0,'C');
	$pdf->Cell(17,15,"LastUpTime",1,0,'C');
	$pdf->Cell(17,15,"StatUpTime",1,0,'C');
	$pdf->Cell(17,15,"Service Type",1,0,'C');
	$pdf->Cell(10,15,"Remark",1,0,'C');
	$pdf->Cell(10,15,"Count",1,1,'C');

	foreach($subs_src as $row)
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$pdf->SetFont("Arial","B",8);
		$pdf->Cell(7,10,$row['id'],1,0,'C');
		$pdf->Cell(11,10,$row['msisdn'],1,0,'C');
		$pdf->Cell(11,10,$row['plan_id'],1,0,'C');
		$pdf->Cell(13,10,$row['keyword'],1,0,'C');
		$pdf->Cell(15,10,$row['shortcode'],1,0,'C');
		$pdf->Cell(15,10,$row['initial_status'],1,0,'C');
		$pdf->Cell(16,10,$row['previous_status'],1,0,'C');
		$pdf->Cell(17,10,$row['current_status'],1,0,'C');
		$pdf->Cell(16,10,$row['final_status'],1,0,'C');
		$pdf->Cell(17,10,$row['incomming_time'],1,0,'C');
		$pdf->Cell(17,10,$row['last_update_time'],1,0,'C');
		$pdf->Cell(17,10,$row['status_update_time'],1,0,'C');
		$pdf->Cell(17,10,$row['service_type'],1,0,'C');
		$pdf->Cell(10,10,$row['remarks'],1,0,'C');
		$pdf->Cell(10,10,$row['count'],1,1,'C');
	}

	$pdf->Ln();
	$pdf->Ln();

	$pdf->Output();

?>