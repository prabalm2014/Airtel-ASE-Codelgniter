<?php
  include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
  <title>Add User</title>
  <!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
  <div class="container">
    <h1 class="col-md-offset-4">Add New User</h1>
    <div class="row">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
            <div class="col-md-4 col-md-offset-4">
                <div class="form-group">
                    <input type="text" name="fname" class="form-control" placeholder="Full Name">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                </div>
                <div class="form-group">
                    <select name="type" class="form-control">
                      <option value="" disabled="disabled" selected>User Type</option>
                      <option value="admin">Admin</option>
                      <option value="reporter">Reporter</option>
                      <option value="support">Supporter</option>
                    </select>
                </div>
                <div class="form-group">
                  <input type="password" name="pass" class="form-control" placeholder="Password">
                </div>
                <span style="color:red;"><?php echo validation_errors(); ?></span>
                <div class="col-md-2 col-md-offset-3">
                  <a href="http://localhost/airtel/airtel/home" align="center"><button type="button" class="btn btn-primary">Back</button></a>
                </div>
                <div class="col-md-1">
                  <input type="submit" name="submit" class="btn btn-primary" value="Sign Up">
              </div> 
            </div>
        </form>
      </div>
    </div>
</body>
</html>