<?php
	require("pdf/fpdf.php");
	$pdf = new FPDF('P','mm','A4');
	$pdf->SetMargins(0,0);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	

	$pdf->SetFont("Arial","B",12);
	$pdf->Cell(0,10,"Service Report",0,1,'C');
	//$pdf->Ln();

	$pdf->SetFont("Arial","B",8);
	$pdf->Cell(8,15,"ID",1,0,'C');
	$pdf->Cell(20,15,"Msisdn",1,0,'C');
	$pdf->Cell(25,15,"Keyword",1,0,'C');
	$pdf->Cell(20,15,"Plan ID",1,0,'C');
	$pdf->Cell(25,15,"Request Type",1,0,'C');
	$pdf->Cell(30,15,"Request Data",1,0,'C');
	$pdf->Cell(30,15,"Response Data",1,0,'C');
	$pdf->Cell(25,15,"Hit Time",1,0,'C');
	$pdf->Cell(25,15,"Response Time",1,1,'C');

	foreach($req_src as $row)
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$pdf->SetFont("Arial","B",8);
		$pdf->Cell(8,15,$row["id"],1,0,'C');
		$pdf->Cell(20,15,$row["msisdn"],1,0,'C');
		$pdf->Cell(25,15,$row["keyword"],1,0,'C');
		$pdf->Cell(20,15,$row["plan_id"],1,0,'C');
		$pdf->Cell(25,15,$row["request_type"],1,0,'C');
		$pdf->Cell(30,15,$row["request_data"],1,0,'C');
		$pdf->Cell(30,15,$row["response_data"],1,0,'C');
		$pdf->Cell(25,15,$row["hittime"],1,0,'C');
		$pdf->Cell(25,15,$row["response_time"],1,1,'C');
	}
	$pdf->Ln();
	$pdf->Ln();

	$pdf->Output();
?>