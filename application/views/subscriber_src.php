<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Subscribers Report</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<!-- for search -->
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
		    		<h2>Subscribers Report</h2>
		        </div>
		</div><br/>
	    <div class="row">
	        <form method="post" action="">
	        	<div class="col-md-2 col-md-offset-2">
					<div class="form-group">
						<input type="text" name="msisdn" class="form-control" placeholder="Msisdn">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="pid" class="form-control" placeholder="Plan ID">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" placeholder="Keyword">
					</div>
	        	</div>
	        	<div class="col-md-2">
					<div class="form-group">
						<select name="stype" class="form-control">
							<option value="" disabled="disabled" selected>Service Type</option>
							<option value="call">CALL</option>
							<option value="sms">SMS</option>
							<option value="voice sms">VOICE SMS</option>
						</select>
					</div>
	        	</div>
	        	<div class="col-md-1">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
	        	</div>
	        </form>
	    </div>
	    </div>
	    <div class="container-fluid">
	    <!-- search result -->
	    <?php
	    if(empty($this->input->get_post('submit')))
	    {
	    	echo "
			<div class='well well-sm'>
				<h3 style='color:blue;'>Type To Search!</h3>
	      			<span style='color:blue'><i>Hints:</i></span><br/>
	      			<ul>
						<li><em>For better result Msisdn,Plan ID,Service Type should be accurate.</em></li>
					</ul> 
	      		</div>";
	    }
	    ?>
	    <?php 
	    $data = $this->session->userdata('log');
	    if($this->input->get_post('submit'))
	    {
	    	echo "<div class='panel panel-default'>
			<div class='panel-body'>
				<table class='table table-striped' style='word-wrap:break-word;'>
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Plan ID</th>
				       	<th>Keyword</th>
				       	<th>Short Code</th>
				       	<th>Initial Status</th>
				       	<th>Previous Status</th>
				       	<th>Current Status</th>
				       	<th>Final Status</th>
				       	<th>Incomming Time</th>
				       	<th>Last Update Time</th>
				       	<th>Status Update Time</th>
				       	<th>Service Type</th>
				       	<th>Remarks</th>
				       	<th>Count</th>";
				       	if(!empty($data['email']) && ($row['type'] == 'admin'))
						{
				       		echo '<th>Action</th>';
				       	}
				    echo "</tr>
			    </thead>
				<tbody>

				{subs_src}
					<tr>
						<td>{id}</td>
						<td>{msisdn}</td>
						<td>{plan_id}</td>
						<td>{keyword}</td>
						<td>{shortcode}</td>
						<td>{initial_status}</td>
						<td>{previous_status}</td>
						<td>{current_status}</td>
						<td>{final_status}</td>
						<td>{incomming_time}</td>
						<td>{last_update_time}</td>
						<td>{status_update_time}</td>
						<td>{service_type}</td>
						<td>{remarks}</td>
						<td>{count}</td>";
						if(!empty($data['email']) && ($row['type'] == 'admin'))
						{
							echo "<td><a href='http://localhost/airtel/airtel/subscriber_upconf/{id}'>Update</a></td>";
						}	
					echo "</tr>
					{/subs_src}
				</tbody>
			</table>";
		}
	   	?>
	   	<form action="http://localhost/airtel/airtel/subscriber_excel" method="post">
			<input type="submit" name="xl" class="btn btn-success" value="Excel">
			<a href='http://localhost/airtel/airtel/subscriber_pdf' class="btn btn-success">PDF</a>
			<a href='http://localhost/airtel/airtel/home' class="btn btn-primary">Back</a>
		</form>
	</div>
</body>
</html>