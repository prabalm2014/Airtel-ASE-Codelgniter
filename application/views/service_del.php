<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Service Delete List</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<div class="container">
	<?php
		$datamsg = $this->session->userdata('sucMsg'); 
		if(!empty($datamsg))
		{
			echo $datamsg['suc'];
		}
		if(!empty($datamsg))
		{
			//$this->session->sess_destroy();
			$this->session->unset_userdata('sucMsg');
		}
	?>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Service Delete List</h1></div>
			<div class="panel-body">
				<table class="table table-striped">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Service Name</th>
				       	<th>Keyword</th>
				       	<th>Base Price</th>
				       	<th>Short Code</th>
				       	<th>Service Node</th>
				       	<th>Plan ID</th>
				       	<th>Service Type</th>
				       	<th>Plan Description</th>
				       	<th>Active Day</th>
				       	<th>Active?</th>
				       	<th>Service ID</th>
				       	<th>Delete</th>
				    </tr>
			    </thead>
			    <tbody>
			    {serv_del}
					<tr>
						<td>{id}</td>
						<td>{service_name}</td>
						<td>{keyword}</td>
						<td>{base_price}</td>
						<td>{shortcode}</td>
						<td>{service_node}</td>
						<td>{plan_id}</td>
						<td>{service_type}</td>
						<td>{plan_desc}</td>
						<td>{active_days}</td>
						<td>{is_active}</td>
						<td>{service_id}</td>
						<td><a href="http://localhost/airtel/airtel/service_del_conf/{id}">Yes</a></td>
					</tr>
				{/serv_del}
			    </tbody>
			  	</table>
			  	<a href="http://localhost/airtel/airtel/home" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>