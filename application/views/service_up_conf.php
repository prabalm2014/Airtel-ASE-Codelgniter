<?php
  include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Update Service</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
<div class="container">
    <h1>Update Service</h1>
    <div class="row">
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>">
          <div class="col-md-6">
          		<div class="form-group">
                  	<label>ID:</label>
                  	<input type="text" name="ids" class="form-control" required readonly value="{id}">
              	</div>
              <div class="form-group">
                  <label>Service Name:</label>
                  <input type="text" name="sname" class="form-control" required value="{service_name}">
              </div>
              <div class="form-group">
                  <label>Keyword:</label>
                  <input type="text" name="keyword" class="form-control" required value="{keyword}">
              </div>
              <div class="form-group">
                  <label>Base Price:</label>
                  <input type="text" name="bprice" class="form-control" required value="{base_price}">
              </div>
              <div class="form-group">
                  <label>Short Code:</label>
                  <input type="text" name="shcode" class="form-control" required value="{shortcode}">
              </div>
              <div class="form-group">
                  <label>Service Node:</label>
                  <input type="text" name="snode" class="form-control" required value="{service_node}">
              </div>
          </div>
          <div class="col-md-6">
          <div class="form-group">
                  <label>Plan ID:</label>
                  <input type="text" name="pid" class="form-control" required value="{plan_id}">
              </div>
              <div class="form-group">
                  <label>Service Type:</label>
                  <input type="text" name="stype" class="form-control" required value="{service_type}">
              </div>
              <div class="form-group">
                  <label>Active Day:</label>
                  <input type="text" name="activday" class="form-control" required value="{active_days}">
              </div>
              
              <div class="form-group">
                  <label>Service ID:</label>
                  <input type="text" name="sid" class="form-control" required value="{service_id}">
              </div>
              <input type="submit" name="submit" class="btn btn-default" value="Submit"><a href="http://localhost/airtel/airtel/service_report" align="center">Back</a>
          </div>
      </form> 
    </div>
</div>
</body>
</html>