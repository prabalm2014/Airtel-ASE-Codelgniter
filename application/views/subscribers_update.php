<?php
	include 'nav.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Subscribers List Update</title>
	<!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">-->
</head>
<body>
	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading"><h1>Subscribers List Update</h1></div>
			<div class="panel-body">
				<table class="table table-striped">
			    <thead>
				    <tr>
				       	<th>ID</th>
				       	<th>Msisdn</th>
				       	<th>Plan ID</th>
				       	<th>Keyword</th>
				       	<th>Short Code</th>
				       	<th>Initial Status</th>
				       	<th>Previous Status</th>
				       	<th>Current Status</th>
				       	<th>Final Status</th>
				       	<th>Incomming Time</th>
				       	<th>Last Update Time</th>
				       	<th>Status Update Time</th>
				       	<th>Service Type</th>
				       	<th>Remarks</th>
				       	<th>Count</th>
				       	<th>Update</th>
				    </tr>
			    </thead>
			    <tbody>
			    {sub_upl}
					<tr>
						<td>{id}</td>
						<td>{msisdn}</td>
						<td>{plan_id}</td>
						<td>{keyword}</td>
						<td>{shortcode}</td>
						<td>{initial_status}</td>
						<td>{previous_status}</td>
						<td>{current_status}</td>
						<td>{final_status}</td>
						<td>{incomming_time}</td>
						<td>{last_update_time}</td>
						<td>{status_update_time}</td>
						<td>{service_type}</td>
						<td>{remarks}</td>
						<td>{count}</td>
						<td><a href="http://localhost/airtel/airtel/subscriber_upconf/{id}">Yes</a></td>
					</tr>
				{/sub_upl}
			    </tbody>
			  	</table>
			  	<a href="http://localhost/airtel/airtel/home" align="center">Back</a>
			</div>
		</div>
  	</div>
</body>
</html>