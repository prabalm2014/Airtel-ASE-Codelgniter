<?php
	require("pdf/fpdf.php");
	$pdf = new FPDF('P','mm','A4');
	$pdf->SetMargins(1,1);
	$pdf->AliasNbPages();
	$pdf->AddPage();
	

	$pdf->SetFont("Arial","B",12);
	$pdf->Cell(0,10,"Service Report",0,1,'C');
	//$pdf->Ln();

	$pdf->SetFont("Arial","B",8);
	$pdf->Cell(8,15,"ID",1,0,'C');
	$pdf->Cell(20,15,"Service Name",1,0,'C');
	$pdf->Cell(15,15,"Keyword",1,0,'C');
	$pdf->Cell(18,15,"Base Price",1,0,'C');
	$pdf->Cell(18,15,"Short Code",1,0,'C');
	$pdf->Cell(20,15,"Service Node",1,0,'C');
	$pdf->Cell(15,15,"Plan ID",1,0,'C');
	$pdf->Cell(20,15,"Service Type",1,0,'C');
	$pdf->Cell(25,15,"Plan Description",1,0,'C');
	$pdf->Cell(18,15,"Active Day",1,0,'C');
	$pdf->Cell(14,15,"Active?",1,0,'C');
	$pdf->Cell(15,15,"Service ID",1,1,'C');


	foreach($srvc_src as $row)
	{
		error_reporting(E_ALL & ~E_NOTICE); 
		$pdf->SetFont("Arial","B",8);
		$pdf->Cell(8,15,$row["id"],1,0,'C');
		$pdf->Cell(20,15,$row["service_name"],1,0,'C');
		$pdf->Cell(15,15,$row["keyword"],1,0,'C');
		$pdf->Cell(18,15,$row["base_price"],1,0,'C');
		$pdf->Cell(18,15,$row["shortcode"],1,0,'C');
		$pdf->Cell(20,15,$row["service_node"],1,0,'C');
		$pdf->Cell(15,15,$row["plan_id"],1,0,'C');
		$pdf->Cell(20,15,$row["service_type"],1,0,'C');
		$pdf->Cell(25,15,$row["plan_desc"],1,0,'C');
		$pdf->Cell(18,15,$row["active_days"],1,0,'C');
		$pdf->Cell(14,15,$row["is_active"],1,0,'C');
		$pdf->Cell(15,15,$row["service_id"],1,1,'C');
	}

	$pdf->Ln();
	$pdf->Ln();

	$pdf->Output();
?>