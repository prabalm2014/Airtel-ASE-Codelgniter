<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AirModel extends CI_Model {
	public function login($data) 
	{
		$sql = "select email,password from user where email= ? and password= ? ";
		$this->load->database();
		$result = $this->db->query($sql, array($data['email'], $data['pass']));
		return $result->row_array(); 
	}
	public function checkUser()//check user for nav bar
	{
		$data = $this->session->userdata('log'); 
		//$data['email'];
		$sql = "select type from user where email='".$data['email']."'";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->row_array(); 
	}
	public function mstatus_report()  //status report list
	{
		$sql = 'SELECT * FROM ase_status ORDER BY id DESC';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function stat_upl() //status update and delete model
	{
		$sql = 'SELECT * FROM ase_status ORDER BY id DESC';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function servc_upl() //service update and delete model
	{
		$sql = 'SELECT * FROM service_list ORDER BY id DESC';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function subs_upl()  //subscriber update
	{
		$sql = 'SELECT * FROM subscribers ORDER BY id DESC';
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->result_array();
	}
	public function service_search($data) //service search....
	{
		$sql = "SELECT * FROM service_list where keyword like ? or shortcode like ? and service_type like ? and plan_id like ? and service_id like ? ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['keyword'], $data['shcode'], $data['type'], $data['pid'], $data['sid'], ));
		return $result->result_array();
	}
	public function callback_search($data) //callback search
	{
		$sql = "SELECT * FROM callback_logs where msisdn like ? and serviceId like ? or planId like ? and serviceType like ? ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['sid'], $data['pid'], $data['stype']));
		return $result->result_array();
	}
	public function request_search($data) //request search
	{
		$sql = "SELECT * FROM request_logs where msisdn like ? or plan_id like ? and hittime like ? and response_time like ?  ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['pid'], $data['hitdate'], $data['resdate']));
		return $result->result_array();
	}
	public function subscriber_search($data) //subscriber search
	{
		$sql = "SELECT * FROM subscribers where msisdn like ? or plan_id like ? and keyword like ? and service_type like ?  ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['pid'], $data['keyword'], $data['stype']));
		return $result->result_array();
	}

	//status update start
	public function status_up_data($id)
	{
		$sql = "SELECT * FROM ase_status WHERE id='$id'";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->row_array(); 
	}
	public function status_upconf($data)
	{
		$this->load->database();
		$sql = "UPDATE ase_status SET status_code_ase = '".$data['asecode']."', status_code_ssl = '".$data['sslcode']."', detaiuls = '".$data['details']."' WHERE id ='".$data['ids']."' ";
		$this->db->query($sql);
	}//status update end

	//service update start
	public function service_up_data($id)
	{
		$sql = "SELECT * FROM service_list WHERE id='$id'";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->row_array(); 
	}

	public function service_upconf($data)
	{
		$this->load->database();
		$sql = "UPDATE service_list SET service_name = '".$data['sname']."', keyword = '".$data['keyword']."', base_price = '".$data['bprice']."', shortcode = '".$data['shcode']."', service_node = '".$data['snode']."', plan_id = '".$data['pid']."', service_type = '".$data['stype']."', active_days = '".$data['activday']."', service_id = '".$data['sid']."' WHERE id ='".$data['ids']."' ";
		$this->db->query($sql);
	}//service update end

	//subscriber update start
	public function subscriber_up_data($id)
	{
		$sql = "SELECT * FROM subscribers WHERE id='$id'";
		$this->load->database();
		$result = $this->db->query($sql);
		return $result->row_array(); 
	}
	public function subscriber_upconf($data)
	{
		$this->load->database();
		$sql = "UPDATE subscribers SET keyword = '".$data['keyword']."', shortcode = '".$data['shcode']."', initial_status = '".$data['instatus']."', previous_status = '".$data['pstatus']."', current_status = '".$data['cstatus']."', final_status = '".$data['fstatus']."' WHERE id ='".$data['ids']."' ";
		$this->db->query($sql);
	}//subscriber update end


	//status delete
	public function status_delconf($id)
	{
		$this->load->database();
		$sql = "DELETE FROM ase_status WHERE id='$id' ";
		$this->db->query($sql);
	}

	//service delete
	public function service_delconf($id)
	{
		$this->load->database();
		$sql = "DELETE FROM service_list WHERE id='$id' ";
		$this->db->query($sql);
	}

	//insert status
	public function ststus_ins($data)
	{
		$this->load->database();
		$sql = "INSERT INTO ase_status VALUES (null,'$data[asecode]', '$data[sslcode]','$data[details]')";
		$this->db->query($sql);
	}

	//insert service
	public function service_ins($data)
	{
		$this->load->database();
		$sql = "INSERT INTO service_list VALUES (null,'$data[sname]', '$data[keyword]','$data[bprice]', '$data[shcode]', '$data[snode]', '$data[pid]', '$data[stype]', '$data[plndesc]','$data[activday]',null,'$data[sid]' )";
		$this->db->query($sql);
	}

	//excel report start from here
	public function service_exc()//service excel
	{
		$data = $this->session->userdata('service'); 
		$sql = "SELECT * FROM service_list where keyword like ? or shortcode like ? and service_type like ? and plan_id like ? and service_id like ? ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['keyword'], $data['shcode'], $data['type'], $data['pid'], $data['sid'] ));
		return $result->result_array();
	}

	public function callback_exc()//callback excel
	{
		$data = $this->session->userdata('callback'); 
		$sql = "SELECT * FROM callback_logs where msisdn like ? and serviceId like ? or planId like ? and serviceType like ? ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['sid'], $data['pid'], $data['stype']));
		return $result->result_array();
	}

	public function request_exc()//request excel
	{
		$data = $this->session->userdata('request'); 
		$sql = "SELECT * FROM request_logs where msisdn like ? or plan_id like ? and hittime like ? and response_time like ?  ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['pid'], $data['hitdate'], $data['resdate']));
		return $result->result_array();
	}
	public function subscriber_exc()//subscriber
	{
		$data = $this->session->userdata('subscriber'); 
		$sql = "SELECT * FROM subscribers where msisdn like ? or plan_id like ? and keyword like ? and service_type like ?  ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['pid'], $data['keyword'], $data['stype']));
		return $result->result_array();
	}//excel end


	//pdf start from here
	public function service_pdf()//service pdf
	{
		$data = $this->session->userdata('service'); 
		$sql = "SELECT * FROM service_list where keyword like ? or shortcode like ? and service_type like ? and plan_id like ? and service_id like ? ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['keyword'], $data['shcode'], $data['type'], $data['pid'], $data['sid'] ));
		return $result->result_array();
	}
	public function callback_pdf()//callback pdf
	{
		$data = $this->session->userdata('callback'); 
		$sql = "SELECT * FROM callback_logs where msisdn like ? and serviceId like ? or planId like ? and serviceType like ? ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['sid'], $data['pid'], $data['stype']));
		return $result->result_array();
	}
	public function request_pdf()//request pdf
	{
		$data = $this->session->userdata('request'); 
		$sql = "SELECT * FROM request_logs where msisdn like ? or plan_id like ? and hittime like ? and response_time like ?  ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['pid'], $data['hitdate'], $data['resdate']));
		return $result->result_array();
	}
	public function subscriber_pdf()//subscriber pdf
	{
		$data = $this->session->userdata('subscriber'); 
		$sql = "SELECT * FROM subscribers where msisdn like ? or plan_id like ? and keyword like ? and service_type like ?  ORDER BY id DESC";
		$this->load->database();
		$result = $this->db->query($sql, array($data['msisdn'], $data['pid'], $data['keyword'], $data['stype']));
		return $result->result_array();
	}//pdf end

	public function addUser($data)
	{
		$this->load->database();
		$sql = "INSERT INTO user VALUES (null,?, ?, ?, ?)";
		$this->db->query($sql , array($data['fname'], $data['email'], $data['type'], $data['pass']));
	}

	//user password update 
	public function password_update($data)
	{
		$dat = $data['npass'];
		$data = $this->session->userdata('log');
		$sql = "UPDATE user SET password = '$dat' WHERE email ='".$data['email']."' ";
		$this->load->database();
		$this->db->query($sql);
	}

	//$sql = "SELECT * FROM some_table WHERE id = ? AND status = ? AND author = ?"; 
	//$this->db->query($sql, array(3, 'live', 'Rick'));
}