<!DOCTYPE html>
<html>
<head>
	<title>Airtel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="http://localhost/airtel/public/style.css" rel="stylesheet" />
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
	  	<div class="container">
	    <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			</button>
			<a class="navbar-brand" href="/airtel/airtel/index">Airtel</a>
	    </div>
	    <div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#" class="fa fa-user" data-toggle="modal" data-target="#mylog">Login</a></li>
			</ul>
	    </div>
	  	</div>
	</nav>
	<div class="bgimg-1">
	  	<div class="caption">
	    	<span class="border">Airtel</span>
	  	</div>
	</div>

	<div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
		<h3 style="text-align:center;">About Us</h3>
		<p> Airtel Bangladesh Limited is one of the fastest growing mobile services providers in Bangladesh and is a concern of Bharti Airtel Limited, a leading global telecommunications services provider. The company offers a wide array of innovative mobile services, including voice, value added services, data and m-commerce products and is focused on expanding its state-of-the-art mobile network both for coverage and capacity.
		With a customer base of more than 10 million, Airtel Bangladesh is the most preferred youth brand of the country that thrives on excellent data service. To make customer's lives easier Airtel Bangladesh has Doorstep Service by which customers can enjoy all kinds of service at their preferred place. M-Commerce opened a new horizon in money transfer that gives Airtel customers the freedom to send money to their dear ones instantly from their mobile. Through M-health, customers can now reach professional doctors over phone 24/7 and get basic treatment.
		To enrich the lives of the customers Airtel has 9 Airtel Experience Centers (AEC) and 58 Airtel Relationship Centers (ARC) across the country and our corporate office is situated in Banani (house 34, Road 19/A), Dhaka 1213, Bangladesh) </p>
	</div>

	<div class="bgimg-2">
		<div class="caption">
			<span class="border" style="background-color:transparent;font-size:25px;color: #f7f7f7;">LESS HEIGHT</span>
		</div>
	</div>

	<div style="position:relative;">
	  	<div style="color:#ddd;background-color:#282E34;text-align:center;padding:50px 80px;text-align: justify;">
	    	<h3 style="text-align:center;">COMPANY PROFILE</h3>
	    	<p style="text-align:center;"> We are India’s largest telecom company. The 200+ million strong and growing airtel family is built on strong values and code of ethics. Know more about us below.</p>
	  	</div>
	</div>

	<div class="bgimg-3">
	  	<div class="caption">
	    	<span class="border" style="background-color:transparent;font-size:25px;color: #f7f7f7;">SCROLL UP</span>
	  	</div>
	</div>

	<div style="position:relative;">
	  	<div style="color:#ddd;background-color:#282E34;text-align:center;padding:50px 80px;text-align: justify;">
	  		<h3 style="text-align:center;">JOBS NEVER DONE BEFORE</h3>
	    	<p style="text-align:center;">One in every 25 people on this planet is served by Airtel. We are a true Indian MNC, operating out of 20 countries across Asia and Africa. Here, we put our customers at the heart of everything we do. We encourage our people to push boundaries and evolve from skilled professionals of today to risk-taking entrepreneurs of tomorrow. We hire people from every realm and offer them opportunities that encourage individual and professional growth. Here, at the world's third largest mobile operator, you will be working with the best minds in the business, to deliver solutions that truly 'enrich the lives of our customers'.

We are always looking for people who are thinkers & doers. People with passion, curiosity & conviction, people who are eager to break away from conventional roles and do 'jobs never done before'.</p>
	 	</div>
	</div>

	<div class="bgimg-2">
	  	<div class="caption">
	    	<span class="border">THANK YOU!</span>
	  	</div>
	</div>
	<!-- log in -->
<div class="container">
  <div class="modal fade" id="mylog" role="dialog">
  <div class="modal-dialog mod">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h1 class="modal-title">Log In</h1>
    </div>
    <div class="modal-body">
        <form method="post" action="/airtel/airtel/login">
            <div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" required placeholder="Email" value="<?php echo set_value('email'); ?>">
                </div>
                <div class="form-group">
                    <input type="password" name="pass" class="form-control" required placeholder="Password">
                </div>
                <span class="err"><?php echo validation_errors(); ?></span>
                <input type="submit" name="submit" class="btn btn-primary" value="Login">
            </div>
        </form>
    </div>
    </div>
  </div>
  </div>
</div>
</body>
</html>